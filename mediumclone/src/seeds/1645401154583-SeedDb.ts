import {MigrationInterface, QueryRunner} from "typeorm";

export class SeedDb1645401154583  implements MigrationInterface {
    name = 'SeedDb1645401154583'

    public async up(queryRunner: QueryRunner): Promise<void> {

        await queryRunner.query(`
INSERT INTO tags(name) VALUES
    ('huskies'),
    ('dragons'),
    ('nestjs')
;`);

        // password is 123
        await queryRunner.query(`
INSERT INTO users(username, email, password) VALUES
    ('tony', 'tony@gmail.com', '$2b$10$CPyteF4njm0RdcIIFruMPue6wdYbBrZK1HR1SMGkhirHs11E./hmS'),
    ('rosie', 'rosie@gmail.com', '$2b$10$CPyteF4njm0RdcIIFruMPue6wdYbBrZK1HR1SMGkhirHs11E./hmS'),
    ('dani', 'dani@gmail.com', '$2b$10$CPyteF4njm0RdcIIFruMPue6wdYbBrZK1HR1SMGkhirHs11E./hmS'),
    ('alex', 'alex@gmail.com', '$2b$10$CPyteF4njm0RdcIIFruMPue6wdYbBrZK1HR1SMGkhirHs11E./hmS')
;`);

await queryRunner.query(`
INSERT INTO articles(slug, title, description, body, "tagList", "authorId") VALUES
    (
        'first-article',
        'First article description',
        'This is my first article',
        'First article body',
        'dragons,nestjs',
        1
    ),
    (
        'second-article',
        'Second article description',
        'This is my second article',
        'Second article body',
        'dragons,huskies',
        1
    )
;`);

    }

    public async down(queryRunner: QueryRunner): Promise<void> {}

}
