import { UpdateUserDto } from './dto/updateUser.dto';
import { JWT_SECRET } from "@app/config";
import { CreateUserDto } from "@app/user/dto/createUser.dto";
import { LoginUserDto } from '@app/user/dto/loginUser.dto';
import { UserResponseInterface } from "@app/user/types/userResponse.interface";
import { UserEntity } from '@app/user/user.entity';
import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { InjectRepository } from '@nestjs/typeorm';
import { hash, compare } from "bcrypt";
import { sign } from 'jsonwebtoken';
import { ObjectID, Repository } from 'typeorm';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>
  ) { }

  async createUser(createUserDto: CreateUserDto): Promise<UserEntity> {
    const errorResponse = {
      errors: {}
    };
    const userByEmail = await this.userRepository.findOne({ email: createUserDto.email });
    const userByUsername = await this.userRepository.findOne({ username: createUserDto.username });

    if (userByEmail) {
      errorResponse.errors['email'] = 'email has already been taken';
    }
    if (userByUsername) {
      errorResponse.errors['username'] = 'username has already been taken';
    }

    if (userByEmail || userByUsername) {
      throw new HttpException(errorResponse, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    const newUser = new UserEntity();
    Object.assign(newUser, createUserDto);
    return await this.userRepository.save(newUser);
  }

  async updateUser(userId: number, updateUserDto: UpdateUserDto): Promise<UserEntity> {
    const userById = await this.findById(userId);

    Object.assign(userById, updateUserDto);
    return await this.userRepository.save(userById);
  }

  async findById(id: any): Promise<UserEntity> {
    return this.userRepository.findOne(id);
  }

  async loginUser(loginUserDto: LoginUserDto): Promise<UserEntity> {
    const errorResponse = {
      errors: {
        'email or password': 'invalid login'
      }
    };

    const user = await this.userRepository.findOne(
      { email: loginUserDto.email },
      { select: ['id', 'username', 'email', 'bio', 'image', 'password'] }
    );
    if (!user) {
      throw new HttpException(errorResponse, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    const isPasswordCorrect = await compare(loginUserDto.password, user.password);
    if (!isPasswordCorrect) {
      throw new HttpException(errorResponse, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    delete user.password;

    return user;
  }

  generateJwt(user: UserEntity): string {
    return sign({
      id: user.id,
      username: user.username,
      email: user.email
    }, JWT_SECRET);
  }

  buildUserResponse(user: UserEntity): UserResponseInterface {
    return {
      user: {
        ...user,
        token: this.generateJwt(user)
      }
    }
  }
}