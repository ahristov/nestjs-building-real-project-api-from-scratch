import { createParamDecorator, ExecutionContext } from "@nestjs/common";

/**
 * Returns current user information.
 *
 * @param {any} data optional name of property of user
 *
 * Usage example: `@User() user: UserEntity`
 * To get only a part of the user data, specify the property name like this: `@User('id') user: number`.
 */
export const User = createParamDecorator((data: any, ctx: ExecutionContext) => {
  const request = ctx.switchToHttp().getRequest();

  if (!request.user) {
    return null;
  }

  if (data) {
    return request.user[data];
  }

  return request.user;
});