import { ArticleEntity } from "@app/article/article.entity";
import { PersistArticleDto } from "@app/article/dto/persistArticle.dto";
import { ArticleResponseInterface } from '@app/article/types/articleResponse.interface';
import { ArticlesResponseInterface } from "@app/article/types/articlesResponse.interface";
import { FollowEntity } from "@app/profile/follow.entity";
import { UserEntity } from '@app/user/user.entity';
import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import slugify from 'slugify';
import { DeleteResult, getRepository, Repository } from "typeorm";

@Injectable()
export class ArticleService {
  constructor(
    @InjectRepository(ArticleEntity)
    private readonly articleRepository: Repository<ArticleEntity>,
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    @InjectRepository(FollowEntity)
    private readonly followRepository: Repository<FollowEntity>,
  ) { }

  async findAllArticles(
    currentUserId: number,
    query: any
  ): Promise<ArticlesResponseInterface> {
    const queryBuilder = getRepository(ArticleEntity)
      .createQueryBuilder('articles')
      .leftJoinAndSelect('articles.author', 'author');

    if (query.tag) {
      queryBuilder.andWhere('articles.tagList LIKE :tag', {
        tag: `%${query.tag}%`
      })
    }

    if (query.author) {
      const author = await this.userRepository.findOne({
        username: query.author
      });
      if (author) {
        queryBuilder.andWhere('articles.authorId = :id', {
          id: author.id
        })
      } else {
        queryBuilder.andWhere('1=0');
      }
    }

    if (query.favorited) {
      const favoritedBy = await this.userRepository.findOne(
        {
          username: query.favorited
        },
        { relations: ['favorites'] }
      );
      const ids = favoritedBy ? favoritedBy.favorites.map(el => el.id) : [];

      if (ids.length > 0) {
        queryBuilder.andWhere('articles.id IN (:...ids)', { ids });
      } else {
        queryBuilder.andWhere('1=0');
      }
    }

    queryBuilder.orderBy('articles.createdAt', 'DESC');

    const articlesCount = await queryBuilder.getCount();

    if (query.limit) {
      queryBuilder.limit(query.limit);
    }

    if (query.offset) {
      queryBuilder.offset(query.offset);
    }

    const currentUser = await this.userRepository.findOne(currentUserId, {
      relations: ['favorites']
    });
    const favoriteIds = currentUser.favorites.map(el => el.id);

    const articles = await queryBuilder.getMany();
    const articlesWithFavorited = articles.map(article => {
      const favorited = favoriteIds.includes(article.id);
      return { ...article, favorited };
    });

    return {
      articles: articlesWithFavorited,
      articlesCount
    };
  }

  async createArticle(
    author: UserEntity,
    createArticleDto: PersistArticleDto
  ): Promise<ArticleEntity> {
    const newArticle = new ArticleEntity();
    Object.assign(newArticle, createArticleDto);
    if (!newArticle.tagList) {
      newArticle.tagList = [];
    }

    newArticle.slug = this.getSlug(createArticleDto.title);
    newArticle.author = author;

    return await this.articleRepository.save(newArticle);
  }

  async feedOfArticles(
    currentUserId: number,
    query: any
  ): Promise<ArticlesResponseInterface> {
    const follows = await this.followRepository.find({
      followerId: currentUserId
    });

    if (follows.length === 0) {
      return {
        articles: [],
        articlesCount: 0
      };
    }

    const followingUserIds = follows.map(follow => follow.followingId);
    const queryBuilder = getRepository(ArticleEntity)
      .createQueryBuilder('articles')
      .leftJoinAndSelect('articles.author', 'author')
      .where('articles.authorId IN (:...ids)', { ids: followingUserIds });

    queryBuilder.orderBy('articles.createdAt', 'DESC');

    const articlesCount = await queryBuilder.getCount();

    if (query.limit) {
      queryBuilder.limit(query.limit);
    }

    if (query.offset) {
      queryBuilder.offset(query.offset);
    }

    const articles = await queryBuilder.getMany();

    return {
      articles,
      articlesCount,
    };
  }

  async updateArticle(
    slug: string,
    updateArticleDto: PersistArticleDto,
    currentUserId: number
  ): Promise<ArticleEntity> {
    const articleFromDb = await this.findArticleBySlug(slug);

    if (!articleFromDb) {
      throw new HttpException('Article does not exist', HttpStatus.NOT_FOUND);
    }

    if (articleFromDb.author.id !== currentUserId) {
      throw new HttpException('You are not the author', HttpStatus.FORBIDDEN);
    }

    Object.assign(articleFromDb, updateArticleDto);

    return await this.articleRepository.save(articleFromDb);
  }

  async findArticleBySlug(slug: string): Promise<ArticleEntity> {
    return await this.articleRepository.findOne({ slug });
  }

  async deleteArticle(slug: string, currentUserId: number): Promise<DeleteResult> {
    const articleBySlug = await this.findArticleBySlug(slug);

    if (!articleBySlug) {
      throw new HttpException('Article does not exist', HttpStatus.NOT_FOUND);
    }

    if (articleBySlug.author.id !== currentUserId) {
      throw new HttpException('You are not the author', HttpStatus.FORBIDDEN);
    }

    return await this.articleRepository.delete({ slug });
  }

  async addArticleToFavorites(
    slug: string,
    userId: number
  ): Promise<ArticleEntity> {
    const articleBySlug = await this.findArticleBySlug(slug);
    if (!articleBySlug) {
      throw new HttpException('Article does not exist', HttpStatus.NOT_FOUND);
    }

    const user = await this.userRepository.findOne(userId, {
      relations: ['favorites']
    });

    const isFavorite = user.favorites.findIndex(
      articleInFavorites => articleInFavorites.id === articleBySlug.id
    ) >= 0;

    if (!isFavorite) {
      user.favorites.push(articleBySlug);
      articleBySlug.favoritesCount++;
      await this.userRepository.save(user);
      await this.articleRepository.save(articleBySlug);
    }

    return articleBySlug;
  }

  async deleteArticleFromFavorites(
    slug: string,
    userId: number
  ): Promise<ArticleEntity> {
    const articleBySlug = await this.findArticleBySlug(slug);
    if (!articleBySlug) {
      throw new HttpException('Article does not exist', HttpStatus.NOT_FOUND);
    }

    const user = await this.userRepository.findOne(userId, {
      relations: ['favorites']
    });

    const favoriteIdx = user.favorites.findIndex(
      articleInFavorites => articleInFavorites.id === articleBySlug.id
    );

    if (favoriteIdx >= 0) {
      user.favorites.splice(favoriteIdx, 1);
      articleBySlug.favoritesCount--;
      await this.userRepository.save(user);
      await this.articleRepository.save(articleBySlug);
    }

    return articleBySlug;
  }

  buildArticleResponse(article: ArticleEntity): ArticleResponseInterface {
    return {
      article
    }
  }

  private getSlug(title: string): string {
    return slugify(title, { lower: true }) + '-' + ((Math.random() * Math.pow(36, 6)) | 0).toString(36);
  }
}