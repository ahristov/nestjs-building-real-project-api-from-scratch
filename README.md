# README #

This repo contains code and notes from studying [NestJS - Building Real Project API From Scratch](https://www.udemy.com/course/nestjs-building-real-project-api-from-scratch/).

## Project ##

The project "mediumclone" is a clone of the Medim API and follows the specifiations from the [Realworld.io project](https://github.com/gothinkster/realworld).

To start "mediumclone":

1) Make sure you have a Postgres DB and and adjust the `ormconfig.ts` as needed.

2) Run

```sh
cd mediumclone
npm install
npm run db:migrate
npx nodemon
```

This will start the server in development mode on Linux/Mac.

To run in development mode _on Windows_, use `npx nodemon -- --config nodemon-windows.json` instead.

To run in production mode, use `npm run start:prod`.

Browse to "http://localhost:3000/tags".

## Postgres ##

To capture some of the commands used in this course, connect with:

```bash
sudo -u postgres psql
```

then run:

- `\l` to list databases
- `\du` to list users
- `create database mediumclone;` to create database
- `create user mediumclone with encrypted password '123';` to create user
- `grant all privileges on database mediumclone to mediumclone;` grant access to user on database
- `\c mediumclone` connect to database
- `\dt` see the table objects
- `\d tags` describe table

## Migrations ##

To clean the DB run `npm run db:drop`.

To create migration run `npm run db:create CreateTags`.

To execute migrations run `npm run db:migrate`.

To seed with test data run `npm run db:seed`.

## TypeORM ##

[Select query builder](https://github.com/typeorm/typeorm/blob/master/docs/select-query-builder.md)
[Many-to-many](https://github.com/typeorm/typeorm/blob/master/docs/many-to-many-relations.md)
